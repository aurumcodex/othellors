# Othello Bot

This README is only going to cover the idiosyncrasies of the Othello game,
but implemented in the Rust language, rather than Python, C/C++, or Java.

Any function within an `impl` block is an instance function (similar to a method within a class).

Any function within that same `impl` block that has a `self` at the begining of the argument list
is a function that can be directly called on a struct or trait type. since Rust treats `struct`s
similarly to a `class` in Java. This manner of `self` is not used within this application, due to it 
*directly* accessing the struct, and destroying the old copy of it, since Rust uses borrows and references
to deal with variables. Variables in Rust are also immutable by default (i.e. using `let` only), adding 
`mut` to the variable declaration allows the variable's data to be modified.

A function with a `&self` is a reference to the struct that the function is called on. This just borrows
the struct's data without destroying it in the process.

An `&mut self` in the function's signature means that the struct that the function is called on
is now mutable, assuming that the struct is designed to be mutable and is declared as a mutable variable.
This means that the function can modify the struct's internal data without destroying the original variable
in the process.

Closures are a neat feature that Rust has support for, which are lazily evaluated anonymous function
that can be assigned to a variable. The declaration for a closure uses `||` instead of `()` for arguments.
The arguments don't have to be explicitly typed, unlike a function. These closures can be used for a
multitude of things, and are actually an example of function programming in Rust. These are used in certain
areas as an easy way to get the particular character needed for printing to `stdout`.

A variable prefixed with a single `_` is used for telling the Rust compiler that it can be left unused.
Useful for when you aren't using a variable in the code just yet.