//! # evaluate.rs
//! 
//! A module to assist in certain functions required for the application to work properly.
//!
//! Author: Nathan Adams
//! Date last modified: 2019.10.25

#[path = "values.rs"]
pub mod values;
// pub(crate) mod values; // I ought to look into this syntax later.

use super::{*};
use values::{BLACK, WHITE};

/// A struct type to hold scoring data.
/// 
/// # Fields
/// + **black**: (type: `isize`)
///   The number of black pieces.
/// + **white**: (type: `isize`)
///   The number of white pieces.
/// + **score**: (type: `isize`)
///   The final score of the game. Negative if white wins, positive if black wins.
#[derive(Copy, Clone)]
pub struct Scores {
    pub black: isize,
    pub white: isize,
    pub score: isize,
}

/// Calculates the scores when called.
/// 
/// # Parameters
/// + **game_board**: (type: `[isize; BOARD_SIZE]`)
///   The game board array to determine the number of discs each color has, and what the final score is.
pub fn calculate_scores(game_board: [isize; BOARD_SIZE]) -> Scores {
    let mut black_count: isize = 0;
    let mut white_count: isize = 0;

    for cell in game_board.iter() {
        if cell == &BLACK {
            black_count += 1;
        } else if cell == &WHITE {
            white_count += 1;
        }
    }

    // the r#result means that result can be used as a variable name, even though it might be a keyword
    let r#result: isize = black_count - white_count;

    Scores {
        black: black_count,
        white: white_count,
        score: result
    } // returns a struct with various scores.
}// end calculate_scores()

/// Prints the results of the game.
/// 
/// # Parameters
/// + **scores**: (type: `Scores`)
///   The struct of Scores that contains the data needed for printing the scores.
pub fn print_results(scores: Scores) {
    if scores.black > scores.white {
        println!("C - black wins");
        println!("C - black pieces: {}", scores.black);
        println!("C - white pieces: {}", scores.white);
        println!("{}", scores.black);
    } else if scores.black < scores.white {
        println!("C - white wins");
        println!("C - black pieces: {}", scores.black);
        println!("C - white pieces: {}", scores.white);
        println!("{}", (-1*scores.black));
    } else if scores.black == scores.white {
        println!("C - a tie occured");
        println!("C - black pieces: {}", scores.black);
        println!("C - white pieces: {}", scores.white);
        println!("{}", scores.black);
    }
}// end print_results()