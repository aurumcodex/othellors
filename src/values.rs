//! # values.rs
//! 
//! A module that contains important values.
//!
//! Author: Nathan Adams
//! Date last modified: 2019.11.4

#![allow(dead_code)]

// half minute for now
pub const TIME_LIMIT: u64 = 30;
pub const MAX_TIME_LIMIT: u64 = 600;

pub const MAX_THREADS: usize = 8;

pub const NORTH:  isize = -8;
pub const SOUTH:  isize =  8;
pub const EAST:   isize =  1;
pub const WEST:   isize = -1;
pub const N_EAST: isize = -7;
pub const N_WEST: isize = -9;
pub const S_EAST: isize =  9;
pub const S_WEST: isize =  7;

pub const DIRECTIONS: [isize; 8] = [NORTH, SOUTH, EAST, WEST, N_EAST, N_WEST, S_EAST, S_WEST];

pub const BLACK:    isize =  1;
pub const WHITE:    isize = -1;
pub const UNFILLED: isize =  0;

pub const WEIGHTED: bool = true;

pub const TOP_BORDER: [usize; 8] = [0, 1, 2, 3, 4, 5, 6, 7];
pub const LEFT_BORDER: [usize; 8] = [0, 8, 16, 24, 32, 40, 48, 56];
pub const BOTTOM_BORDER: [usize; 8] = [56, 57, 58, 59, 60, 61, 62, 63];
pub const RIGHT_BORDER: [usize; 8] = [7, 15, 23, 31, 39, 47, 55, 63];

// factors of 5 and/or 3.
pub const WEIGHTS: [isize; 64] = [
        /* _A_  _B_  _C_  _D_  _E_  _F_  _G_  _H_ */
    /*1*/ 150, -30,  30,   5,   5,  30,  -30, 150,
    /*2*/ -30, -50,  -5,  -5,  -5,  -5,  -50, -30,
    /*3*/  30,  -5,  15,   3,   3,  15,   -5,  30,
    /*4*/   5,  -5,   3,   3,   3,   3,   -5,   5,
    /*5*/   5,  -5,   3,   3,   3,   3,   -5,   5,
    /*6*/  30,  -5,  15,   3,   3,  15,   -5,  30,
    /*7*/ -30, -50,  -5,  -5,  -5,  -5,  -50, -30,
    /*8*/ 150, -30,  30,   5,   5,  30,  -30, 150,
];