//! # player.rs
//! 
//! A module to assist in dealing with players (human or other bots).
//!
//! Author: Nathan Adams
//! Date last modified: 2019.11.2

#[path = "bot.rs"]
pub mod bot;

#[path = "values.rs"]
pub mod values;

use std::io;

use crate::bot::{Bot};
use crate::values::{*};

use super::{Move, OthelloBoard};

/// A struct to hold player data.
/// 
/// # Fields
/// + **color**: (type: `isize`)
///   The color that the player/challenger plays as.
/// + **num_discs**: (type: `isize`)
///   The number of discs the player/challenger has.
/// + **score**: (type: `isize`)
///   The score value that the player/challenger has.
/// + **passing**: (type: `bool`)
///   A boolean for the player/challenger determining if it passes or not.
#[derive(Copy, Clone)]
pub struct Player {
	pub color: isize,
	pub num_discs: isize,
	pub score: isize,
    pub passing: bool,
}

impl Player {
    /// Generates a new Player data type.
    pub fn new() -> Player {
        Player {
            color: UNFILLED,
            num_discs: 2,
            score: 0,
            passing: false
        }
    }// end new() constructor

    /// Gets "user" or challenger's input.
    /// 
    /// # Parameters
    /// + **legal_cells**: (type: `Vec<usize>`)
    ///   A Vector of `usize` integers that point to the various cells on the board. Used as a
    ///   list to check if the user's input is in the list.
    pub fn get_move_input(&self, legal_cells: Vec<usize>, is_human: bool) -> usize {
        let mut return_move: usize = usize::max_value();

        let mut row: usize = 0;
        let mut col: usize = 0;

        let mut input: String = String::new();

        if !legal_cells.is_empty() {
            println!("C - challenger has valid moves available.");
        }

        println!("C - enter move (color, column, row)");
        io::stdin().read_line(&mut input).expect("C - unable to read user input");

        let chars: Vec<&str> = (&*input).split_whitespace().collect();

        if (chars[0] == "B" && self.color == BLACK) && !legal_cells.is_empty() {
            if chars.len() > 1 {
                match chars[1] {
                    "a" => col = 0,
                    "b" => col = 1,
                    "c" => col = 2,
                    "d" => col = 3,
                    "e" => col = 4,
                    "f" => col = 5,
                    "g" => col = 6,
                    "h" => col = 7,
                    _ => {},
                }// end match on chars[1]

                match chars[2] {
                    "1" => row = 0,
                    "2" => row = 1,
                    "3" => row = 2,
                    "4" => row = 3,
                    "5" => row = 4,
                    "6" => row = 5,
                    "7" => row = 6,
                    "8" => row = 7,
                    _ => {},
                }// end match on chars[2]

                return_move = (row*8)+col;

                if !legal_cells.contains(&return_move) { 
                    match is_human {
                        true  => {
                            println!("C - since player is human, a rentry of move is allowed. re-enter move.");
                            self.get_move_input(legal_cells, is_human);
                        }, //end true branch
                        false => {
                            panic!("invalid move entered");
                        }// end false branch
                    }// end match for is_human
                }
            }
        } else if (chars[0] == "W" && self.color == WHITE) && !legal_cells.is_empty() {
            if chars.len() > 1 {
                match chars[1] {
                    "a" => col = 0,
                    "b" => col = 1,
                    "c" => col = 2,
                    "d" => col = 3,
                    "e" => col = 4,
                    "f" => col = 5,
                    "g" => col = 6,
                    "h" => col = 7,
                    _ => {},
                }// end match on chars[1]

                match chars[2] {
                    "1" => row = 0,
                    "2" => row = 1,
                    "3" => row = 2,
                    "4" => row = 3,
                    "5" => row = 4,
                    "6" => row = 5,
                    "7" => row = 6,
                    "8" => row = 7,
                    _ => {},
                }// end match on chars[2]

                return_move = (row*8)+col;

                if !legal_cells.contains(&return_move) { 
                    match is_human {
                        true  => {
                            println!("C - since player is human, a rentry of move is allowed. re-enter move.");
                            self.get_move_input(legal_cells, is_human);
                        }, //end true branch
                        false => {
                            panic!("invalid move entered");
                        }// end false branch
                    }// end match for is_human
                }
            }
        } else if chars[0] == "C" {
            // comment handling
            println!("C - a comment was entered; ignoring. please re-enter.");
            self.get_move_input(legal_cells, is_human);
        } else {
            panic!("invalid move was entered: {}", input);
        }

        println!("C - move at cell: {} | {} {} {}", &return_move, self.color, chars[1], chars[2]);
        return_move
    }// end get_input()

    /// Gets "user" input, but causes a pass to occur.
    ///
    /// # Parameters
    /// + **challenger**: (type: `mut Player`)
    ///   The player struct,
    /// + **bot**: (type: `mut Bot`)
    ///   The bot struct.
    pub fn get_pass_input(&self, mut challenger: Player, mut bot: Bot) {
        let mut input = String::new();
        // the `?` operator doesn't work when a function doesn't return anything.
        io::stdin().read_line(&mut input).expect("C - unable to read user input");

        match input.trim() {
            "B" => {
                if challenger.color != BLACK {
                    println!("C - challenger has no valid moves and has to pass. please re-enter.");
                    self.get_pass_input(challenger, bot);
                } else {
                    match challenger.passing {
                        false => challenger.passing = true,
                        true => bot.passing = true,
                    }// end match for passing
                }
            }, // end "B" case
            "W" => {
                if challenger.color != WHITE {
                    println!("C - challenger has no valid moves and has to pass. please re-enter.");
                    self.get_pass_input(challenger, bot);
                } else {
                    match challenger.passing {
                        false => challenger.passing = true,
                        true => bot.passing = true,
                    }// end match for passing
                }
            }, // end "W" case
            _ => {
                println!("C - invalid option found. please re-enter.");
                self.get_pass_input(challenger, bot);
            }
        }// end match for input
    }// end get_pass_input()
}// end Player impl block