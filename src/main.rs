//! # Othello Bot
//! 
//! An AI bot program that is able to play a game of Othello:tm:.
//! 
//! Author: Nathan Adams
//! Date last modified: 2019.11.4

#![allow(non_snake_case)]

#[macro_use]
extern crate clap;
extern crate rand; /* :) */
extern crate rayon;

pub mod board;
pub mod bot;
pub mod evaluate;
pub mod player;
// pub mod timer;
pub mod values;

use std::io;
// use std::thread;
 use std::time;
//use rayon::prelude::{*};

use board::{*};
use values::{*};
use evaluate::{*};
use rand::Rng;

/// The main game loop resides within this function. Effectively the same as a main function
/// in really any other compiled language that has some similarities to C or C++.
fn main() -> io::Result<()> {

    // command line argument parsing.
    let arguments = clap::App::new("Othello Bot.rs")
                    .version(crate_version!())
                    .author(crate_authors!())
                    .about("An AI bot that can play Othello")
                    .arg(
                        clap::Arg::with_name("debug")
                        .short("d")
                        .long("debug")
                        .help("Turns debug printing on.")
                    )
                    .arg(
                        clap::Arg::with_name("time_limit")
                        .short("t")
                        .long("time_limit")
                        .value_name("SECONDS")
                        .help("Sets the time limit of the entire game.")
                    )
                    .arg(
                        clap::Arg::with_name("threads")
                        .short("n")
                        .long("threads")
                        .value_name("THREADS")
                        .help("Sets number of threads for bot to use.")
                    )
                    .arg(
                        clap::Arg::with_name("human")
                        .short("H")
                        .long("human")
                        .help("Sets flag if a human is playing")
                    )
                    .get_matches();

    let use_debug: bool = arguments.is_present("debug");
    let is_human: bool = arguments.is_present("human");
    let mut time_limit: i64 = 0;
    let mut threads: usize = 4; // default to 4 threads.

    if use_debug {
        println!("C - using debug printing");
    } else {
        println!("C - not using debug printing");
    }

    if arguments.is_present("threads") {
        if let Some(thrd) = arguments.value_of("threads") {
            threads = thrd.parse::<usize>().unwrap();
            if threads > MAX_THREADS {
                panic!("maximum thread count cannot exceed 8 threads.");
            }
            println!("C - set thread pool to use {} threads.", threads);
            rayon::ThreadPoolBuilder::new().num_threads(threads).build_global().unwrap();
        }
    } else {
        println!("C - set thread pool to use {} threads. (default)", threads);
        rayon::ThreadPoolBuilder::new().num_threads(threads).build_global().unwrap();
    }

    if arguments.is_present("time_limit") {
        if let Some(time) = arguments.value_of("time_limit") {
            time_limit = time.parse::<i64>().unwrap();
            if time_limit > MAX_TIME_LIMIT as i64 {
                panic!("maximum game time cannot be over 10 minutes. (600 seconds)")
            }
            println!("C - time limit is set to: {:?}", time_limit);
        } 
    } else {
        time_limit = MAX_TIME_LIMIT as i64;
        println!("C - no option for time limit set. using default limit.");
        println!("C - time limit is set to: {:?}", time_limit);
    }

    println!("C - using build version: [v{}]", crate_version!());

    // this is a closure - a sort of functional programming that captures the program's environment.
    let color = |num: isize| -> &str {
        match num {
            BLACK => "B",
            WHITE => "W",
            _ => " ",
        }
    };

    let full_color = |num: isize| -> &str {
        match num {
            BLACK => "black",
            WHITE => "white",
            _ => "",
        }
    };

    let column = |num: usize| -> &str {
        match num % 8 {
            0 => "a",
            1 => "b",
            2 => "c",
            3 => "d",
            4 => "e",
            5 => "f",
            6 => "g",
            7 => "h",
            _ => "_",
        }
    };

    let row = |num: usize| -> &str {
        match num / 8 {
            0 => "1",
            1 => "2",
            2 => "3",
            3 => "4",
            4 => "5",
            5 => "6",
            6 => "7",
            7 => "8",
            _ => "_",
        }
    };

    let direction = |dir: isize| -> &str {
        match dir {
            NORTH => "North",
            SOUTH => "South",
            WEST => "West",
            EAST => "East",
            N_EAST => "North East",
            N_WEST => "North West",
            S_EAST => "South East",
            S_WEST => "South West",
            _ => "(not a valid direction)",
        }
    };

    let mut _move: usize = 0;

    let mut othello: OthelloBoard = OthelloBoard::new();

    println!("C - Enter how to initialize the bot: (I W for white, I B for black)");

    let mut input: String = String::new();
    io::stdin().read_line(&mut input)?; // the `?` is a syntatic sugar for the `.expect("err reason")`
                                        // that usually is suffixed to a line read;
    match input.trim() {
        "I W" => {
            println!("C - initialized player as black.");
            println!("C - initialized bot as white.");
            println!("R W");
            othello.gen_board(WHITE, time_limit, use_debug);
        },
        "I B" => {
            println!("C - initialized player as white.");
            println!("C - initialized bot as black.");
            println!("R B");
            othello.gen_board(BLACK, time_limit, use_debug);
        },
        _ => {
            // this is here should an invalid initialization arrive
            panic!("invalid initialization: {}", input.trim());
        }
    }// end match input

    let mut _current_player = othello.challenger.color;

    if use_debug {
        if othello.challenger.color == WHITE {
            println!("C - player (0) color is white; bot (@) is playing black\nC -");
        } else if othello.challenger.color == BLACK {
            println!("C - \nC - player (0) color is black; bot (@) is playing white");
        }
    }

    let mut turn_count: u8 = 0;

    /* >> main game loop here << */
    'main_loop: while !othello.game_over {
        let mut _legal_move_list: Vec<Move> = Vec::new();
        let mut _legal_cell: Vec<usize> = Vec::new();

        println!("C -\nC - turn count is: {}\nC -", turn_count);

        if _current_player == BLACK {
            _legal_move_list.clear();
            _legal_cell.clear();
            let _legal_move_list = othello.generate_legal_moves(othello.challenger.color);

            othello.print_board_with_moves(&_legal_move_list);

            println!("C - challenger's turn [as {}]", full_color(othello.challenger.color));

            if use_debug {
                println!("C - legal moves:");
            }

            'challenger_moves: for list in &_legal_move_list {
                if use_debug {
                    print!("C - {} {} {} (cell: {}) | ",
                        color(othello.challenger.color), column(list.cell), row(list.cell), list.cell);

                    print!("number of flipped discs: {} | ", list.num_flips);
                    println!("direction: {}", direction(-list.direction));
                }
                _legal_cell.push(list.cell);
            }

            if _legal_move_list.len() == 0 {
                println!("C - challenger has to pass");
                othello.challenger.get_pass_input(othello.challenger, othello.bot);
            } else {
                _move = othello.challenger.get_move_input(_legal_cell, is_human);
                othello.apply_move(othello.challenger.color, _move, use_debug);

                for list in _legal_move_list {
                    if list.cell == _move {
                        othello.flip_coins(othello.challenger.color, -1*list.direction, list.cell, use_debug);
                    }
                }
            }
        } else if _current_player == WHITE {
            _legal_move_list.clear();
            _legal_cell.clear();
            let _legal_move_list = othello.generate_legal_moves(othello.bot.color);

            othello.print_board_with_moves(&_legal_move_list);

            println!("C - bot's turn [as {}]", full_color(othello.bot.color));

            if use_debug {
                println!("C - legal moves:");
            }

            if _legal_move_list.len() == 0 {
                println!("C - bot has to pass");
                println!("{}", color(othello.bot.color));
                if othello.challenger.passing == false {
                    othello.challenger.passing = true;
                } else {
                    othello.bot.passing = true;
                }
            } else {
                for list in &_legal_move_list {
                    if use_debug {
                        print!("C - {} {} {} (cell: {}) | ",
                            color(othello.bot.color), column(list.cell), row(list.cell), list.cell);
                        print!("number of flipped discs: {} | ", list.num_flips);
                        println!("direction: {}", direction(-list.direction));
                    }
                    _legal_cell.push(list.cell);
                }

                let cur_instant = time::Instant::now();
                _move = othello.bot.make_move(&_legal_move_list, othello, turn_count, use_debug);
                let move_time = cur_instant.elapsed().as_secs();

                if use_debug {
                    println!("C - move took {} seconds to generate", move_time);
                }
                othello.time_remaining -= move_time as i64;
                if use_debug {
                    println!("C - time remaining: {} seconds,", othello.time_remaining);
                }

                if !_legal_cell.contains(&_move) {
                    println!("C - the bot seems to have generated an erroneous move. using fallback move.");
                    _move = gen_fallback_move(_legal_cell);
                }// check and make sure that the bot generates a proper move, just to be safe.

                println!("C - bot chose to play at: {} {} {}", color(othello.bot.color), column(_move), row(_move));
                println!("{} {} {}", color(othello.bot.color), column(_move), row(_move));
                othello.apply_move(othello.bot.color, _move, use_debug);

                for list in &_legal_move_list {
                    if list.cell == _move {
                        othello.flip_coins(othello.bot.color, -1*list.direction, list.cell, use_debug);
                    }
                }
            }
        }// end overarching if statements
        
        _current_player = -1*_current_player;
        othello.game_over = othello.is_game_over();

        turn_count += 1;
    }// end while loop

    let scores = calculate_scores(othello.board);
    print_results(scores);
    println!("C - turn count was: {}", turn_count);
    // othello.confirm_game_over(scores);

    Ok(()) // returns an Ok() signal, effectively the same as a `return 0;` in a C main().
           // this return statement is completely optional, but I find it nice to have explicit
           // data types, even though some explicit typing on some variables is left off.

}// end main()

/// Generate a fallback move should the bot come up with an erroneous move.
///
/// # Parameters
/// + **cells**: (type: `Vec<usize>`)
///   A vector of `usize` numbers that make up the valid cells in a move list.
///   Used to determine when to stop the loop.
pub fn gen_fallback_move(cells: Vec<usize>) -> usize {
    let mut rand_move: usize = rand::thread_rng().gen_range(0, BOARD_SIZE);
    while !cells.contains(&rand_move) {
        rand_move = rand::thread_rng().gen_range(0, BOARD_SIZE);
    }
    rand_move
}// end gen_fallback_move()
