//! # bot.rs
//! 
//! A module to assist in certain functions required for the AI bot to function.
//! 
//! Author: Nathan Adams
//! Date last modified: 2019.11.4

extern crate rand; /* :) the bot's gonna be doing some fun stuff now */

#[path = "values.rs"]
pub mod values;

use std::sync::mpsc;
use std::thread;
use std::time;

use rand::{
    distributions::{Bernoulli, Distribution},
    Rng
};

use rayon::prelude::{*};

use crate::board::BOARD_SIZE;
use crate::values::{*};

use super::{Move, OthelloBoard};

/// A struct to hold bot data.
/// 
/// # Fields
/// + **color**: (type: `isize`)
///   The color that the bot plays as.
/// + **num_discs**: (type: `isize`)
///   The number of discs the bot has.
/// + **score**: (type: `isize`)
///   The score value that the bot has.
/// + **passing**: (type: `bool`)
///   A boolean for the bot determining if it passes or not.
#[derive(Copy, Clone)]
pub struct Bot {
	pub color: isize,
	pub num_discs: isize,
	pub score: isize,
    pub passing: bool,
}

impl Bot {
    /// Generates a new Bot data type.
	pub fn new() -> Bot {
        Bot {
            color: UNFILLED,
            num_discs: 2,
            score: 0,
            passing: false
        }
    }// end new() constructor

    
    /// The bread and butter to this bot;
    /// Generates a move that the bot will then apply to the board.
    ///
    /// This function happens to generate two moves currently, and is multithreaded.
    /// 
    /// # Parameters
    /// + **legal_moves**: (type: `&Vec<Move>`)
    ///   A reference to a Vector of Moves which contains all of the legal moves.
    /// + **game**: (type: `OthelloBoard`)
    ///   The game state and board that the bot has to determine its move.
    /// + **debug**: (type: `bool`)
    ///   Enables debug output.
    #[allow(unused_assignments)]
    pub fn make_move(&self, legal_moves: &Vec<Move>, game: OthelloBoard, turn_count: u8, debug: bool) -> usize {

        let (tx, rx): (mpsc::Sender<usize>, mpsc::Receiver<usize>) = mpsc::channel(); // mpsc variable here
                                        // (multiple producer, single consumer multithreaded channels)
        let (tx1, rx1): (mpsc::Sender<usize>, mpsc::Receiver<usize>) = mpsc::channel();

        let mut cell_vec: Vec<usize> = Vec::new();

        // these vectors are here for the threads to work correctly.
        let rng_move_clone = legal_moves.to_vec();
        let w_rng_move_clone = legal_moves.to_vec();

        // a set of variables for generating a nanosecond count, which is used in the flag setting below.
        let rng_instant: time::Instant = time::Instant::now();
        let mut rng_duration: f64 = rng_instant.elapsed().as_nanos() as f64;

        if rng_duration > 1000.0 { rng_duration -= 1000.0; }

        let n_instant: time::Instant = time::Instant::now();
        let n_duration: f64 = n_instant.elapsed().as_nanos() as f64;

        if debug {
            println!("C - rng_duration value is: [{}ns] | probability = {}", rng_duration, (rng_duration/1000.0) + 0.075);
            println!("C - normal_duration value is: [{}ns] | probability = {}", n_duration, (n_duration/1000.0) + 0.775);
            thread::sleep(time::Duration::new(2, 0));
        }

        // a probability of the time in nanoseconds as a floating point number added with 0.25
        // to give the Bernoulli RNG a chance of working that is at minimum, a 1 in 4 chance of
        // a `true` flag.
        let use_rand_dist = Bernoulli::new((rng_duration/1000.0) + 0.15).unwrap();
        let use_random_move = use_rand_dist.sample(&mut rand::thread_rng());
        let use_weighted = Bernoulli::new((n_duration/1000.0) + 0.25).unwrap();
        let use_weighted_move = use_weighted.sample(&mut rand::thread_rng());

        if debug {
            println!("C - using RNG move? {}", use_random_move);
            println!("C - using weighted? {}", use_weighted_move);
            thread::sleep(time::Duration::new(1, 0));
        }

        let depth: usize = 0;
        let is_maxing_player: bool = true;
        let alpha = std::f64::MIN;
        let beta = std::f64::MAX;
        let color = self.color;

        let mut best_move: usize = 0;
        let /*mut*/ best_score: isize = isize::min_value();
        let /*mut*/ ab_temp_score: isize = 0;
        let /*mut*/ _wght_ab_temp_score: isize = 0;
        /* just for "fun": a random move or two are considered. randomly applied. */

        let is_using_weight = |flag| -> &str {
            match flag {
                true  => "(weighted)",
                false => "(non-weighted)"
            }
        };// end closure is_using_weight

        // for loop with label 'cells.
        'cells: for legal in legal_moves {
            if debug {
                println!("C - adding legal move: {}", legal.cell);
                println!("C - legal move direction is: {}", legal.direction);
            }
            cell_vec.push(legal.cell);
        }// end 'cells for loop

        if use_random_move {
            // RNG thread
            let _rng_thread = thread::spawn(move || {
                let dbg_cp = debug;
                let rng_move = gen_random_move(rng_move_clone, !WEIGHTED, dbg_cp);
                
                if dbg_cp {
                    println!("C - rng move is: {:?}", rng_move);
                }
    
                tx.send(rng_move).unwrap();
            });
    
            // weighted RNG thread
            let _weighted_rng_thread = thread::spawn(move || {
                let dbg_cp = debug;
                let rng_move = gen_random_move(w_rng_move_clone, WEIGHTED, dbg_cp);

                if dbg_cp {
                    println!("C - weighted rng move is: {:?}", rng_move);
                }
    
                tx1.send(rng_move).unwrap();
            });
        }// end branch for checking if the bot is to use a random move or not.

        let (ttx, trx): (mpsc::Sender<(usize, isize)>, mpsc::Receiver<(usize, isize)>) = mpsc::channel();

        if !use_random_move {
            println!("C - using alpha_beta search. {}", is_using_weight(use_weighted_move));
            legal_moves.to_vec()
                .par_iter_mut()
                .for_each_with(ttx, move |sender, m| {
                    let mut ab_temp = ab_temp_score.clone();
                    let mut best_s = best_score.clone();
                    let mut best_m = best_move.clone();

                    if debug {
                        println!("C - legal move is: {}", m.cell);
                        println!("C - legal move direction is: {}", m.direction);
                    }// end debug check 1

                    let cell_weight = WEIGHTS[m.cell];

                    let mut temp_board: OthelloBoard = game.clone();
                    temp_board.apply_move(color, m.cell, debug);
                    temp_board.flip_coins(color, -m.direction, m.cell, debug);

                    ab_temp = temp_board.alpha_beta(!is_maxing_player, alpha, beta, -color, depth, turn_count, debug);

                    println!("C - alpha_beta output: {} at cell: {}", ab_temp, m.cell);

                    match use_weighted_move {
                        true => {
                            if debug {
                                println!("C -\nC - using weighted strategy");
                            }// debug check 2

                            if ab_temp > best_s {
                                best_s = WEIGHTS[m.cell];
                                if best_s > cell_weight {
                                    if debug { println!("C -\nC - (weighted) best move is: {}", m.cell); }
                                    best_m = m.cell;
                                }
                                best_m = m.cell;
                            }
                        }, //end true branch
                        false => {
                            if debug {
                                println!("C -\nC - not using weighted stategy");
                            }// debug check 3

                            if ab_temp > best_s {
                                if debug { println!("C -\nC - (non-weighted) best move is: {}", m.cell); }

                                best_s = ab_temp;
                                best_m = m.cell;
                            }
                        }//end false branch
                    }// end match
                    let tp_move = (best_m, best_s);
                    // sender.send(best_m as isize).unwrap();
                    //sender.send(ab_temp).unwrap();
                    sender.send(tp_move).unwrap();
                });// end parallel for loop

            let ab_scores: Vec<_> = trx.iter().collect();
            let cp_ab_scrs = ab_scores.clone();
            if debug { println!("C - vec ab_scores contains: {:?}", &ab_scores); }

            'in_moves: for score in ab_scores {
                let min = cp_ab_scrs.iter().min().unwrap();

                if debug {
                    println!("C - score of :: {:?}", score);
                }

                if score > *min {
                    best_move = cp_ab_scrs[0].0;
                }
            }

            println!("C - best move is: {}", best_move);
            return best_move; // this is the *only* instance of an explicit return statement;
                              // if this isn't here, then the bot doesn't respond, and hangs.
        }// end use_random_move determination

        let recv_rng_move = rx.recv().unwrap() as usize;
        let recv_w_rng_move = rx1.recv().unwrap() as usize;

        match use_random_move {
            true => {
                println!("C -\nC - using a random move instead.");

                if debug {
                    println!("C - cell_vec contains: {:?}", cell_vec);
                }

                match use_weighted_move {
                    true => best_move = recv_w_rng_move,
                    false => best_move = recv_rng_move,
                }
            }, // end true branch
            false => {
                println!("C - using move generated by alpha_beta()."); 
                if debug {
                    println!("C - cell_vec contains: {:?}", cell_vec);
                }
            }, // end false branch
        }// end match

        println!("C - move generated was: {}", best_move);

        best_move
    }// end make_move()
}// end impl block for Bot

/// NOTE: This function is outside of the `impl` block since it's to help one thing,
/// and is not meant to be directly called, and is private to denote such a thing.
/// 
/// Generates a random value for a Move cell.
///
/// # Parameters
/// + **cells**: (type: `Vec<Move>`)
///   The list of valid moves to search through.
/// + **is_weighted**: (type: `bool`)
///   A flag to determine which type of RNG move to generate.
fn gen_random_move(cells: Vec<Move>, is_weighted: bool, debug: bool) -> usize {
    let mut cell_list: Vec<usize> = Vec::new();
    let mut rand_move: usize = rand::thread_rng().gen_range(0, BOARD_SIZE);

    'cells: for _move in cells {
        cell_list.push(_move.cell);
    }// end 'cells for loop

    if !is_weighted {
        if debug {
            println!("C - unsorted cell list: {:?}", cell_list);
        }

        'unweighted: for _ in &cell_list {
            while !cell_list.contains(&rand_move) {
                let _move: usize = rand::thread_rng().gen_range(0, BOARD_SIZE);
                
                if debug {
                    println!("C - non-weighted RNG move = {}", _move);
                }

                rand_move = _move;
            }// end 'unweighted while loop
        }
    } else if is_weighted {
        cell_list.sort_unstable(); // slightly more unsafe sorting of vector; changes how the data in 
                                   // vector is organized.
        if debug {
            println!("C -\nC - sorted cell list: {:?}", cell_list);
        }

        let mut temp: isize = WEIGHTS[cell_list[0]]; // used in 'weighted loop
        println!("C - temp is: {}", temp);

        'weighted: for _ in &cell_list {
            while !cell_list.contains(&rand_move) {
                let _move: usize = rand::thread_rng().gen_range(0, BOARD_SIZE);
                
                if debug {
                    println!("C - weighted RNG move = {}", _move);
                }

                rand_move = _move;
            }
            if WEIGHTS[rand_move] > temp {
                temp = WEIGHTS[rand_move];
                println!("C - sorted cell list: {:?}", cell_list);
            }
        }// end 'weighted loop
    }

    rand_move
}// end gen_random_move()