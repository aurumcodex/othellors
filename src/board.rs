//! # board.rs
//! 
//! A module that handles most of the board logic, among a few other things.
//!
//! Author: Nathan Adams
//! Date last modified: 2019.11.4

// extern crate rayon;

#[path = "evaluate.rs"]
pub mod evaluate;

#[path = "bot.rs"]
pub mod bot;

#[path = "player.rs"]
pub mod player;

#[path = "values.rs"]
pub mod values;

#[path = "tests.rs"]
pub mod tests;

use std::{io, /*thread*/};
// use std::time::Duration;

// use std::sync::{
//     Arc,
//     atomic::{AtomicBool, AtomicU64, Ordering}
// };

// use rayon::prelude::{*};

use crate::bot::{Bot};
use crate::evaluate::{Scores, calculate_scores};
use crate::player::{Player};
use crate::values::{*};

pub const BOARD_SIZE: usize = 64;

/// A struct to hold information about a Move.
///
/// # Fields
/// + **cell**: (type: `usize`)
///   The index on the board where a Move will/would be applied.
/// + **num_flips**: (type: `usize`)
///   The number of discs flipped by a Move.
/// + **direction**: (type: `isize`)
///   The direction that Move is from the origin point of generation.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Move {
    pub cell: usize,
    pub num_flips: usize,
    pub direction: isize,
}

impl Move {
    pub fn get_weight(cell: Move) -> isize {
        println!("C - weight at cell {} = {}", cell.cell, WEIGHTS[cell.cell]);
        WEIGHTS[cell.cell]
    }
}// end impl block for Move


/// A struct to hold some of the game data.
///
/// # Fields
/// + **bot**: (type: `Bot`)
///   The Bot that is what the player/challenger plays against.
/// + **challenger**: (type: `Player`)
///   The Player that is what the Bot plays against.
/// + **board**: (type: `[isize; BOARD_SIZE]`)
///   An array of length 64 of isize integers, which holds the data of the Othello board.
/// + **game_over**: (type: `bool`)
///   A boolean value that denotes if the game is over or not.
#[derive(Copy, Clone)]
pub struct OthelloBoard {
    pub bot: Bot,
    pub challenger: Player,
    pub board: [isize; BOARD_SIZE],
    pub time_remaining: i64,
    pub game_over: bool,
}

// pub trait 

// #[derive(Copy, Clone)]
impl OthelloBoard {
    /// Generates a new OthelloBoard data type.
    pub fn new() -> OthelloBoard {
        OthelloBoard {
            bot: Bot::new(),
            challenger: Player::new(),
            board: [UNFILLED; BOARD_SIZE],
            time_remaining: MAX_TIME_LIMIT as i64,
            game_over: false,
        }
    }// end new()

    /// Modifies the board struct after creation due to how the Rust compiler works.
    /// 
    /// # Parameters
    /// + **init_color**: (type: `isize`)
    ///   The color to initialize the board as.
    pub fn gen_board(&mut self, init_color: isize, time: i64, debug: bool) {
        self.bot.color = init_color;
        self.challenger.color = -1*init_color;

        self.time_remaining = time;

        if debug {
            println!("C - bot number is: {}, init_color was: {}", self.bot.color, init_color);
            println!("C - challenger number is: {}, init_color was: {}",
                      self.challenger.color, -1*init_color);
        }

        self.board[27] = WHITE;
        self.board[28] = BLACK;
        self.board[35] = BLACK;
        self.board[36] = WHITE;
    }// end gen_board()

    /// Applies the given move from the specified color to the board.
    /// 
    /// # Parameters
    /// + **color**: (type: `isize`)
    ///   The color (player) whose Move gets applied to the board.
    /// + **cell**: (type: `usize`)
    ///   The spot on the board to apply the Move.
    pub fn apply_move(&mut self, color: isize, cell: usize, debug: bool) {
        if self.board[cell] == UNFILLED {
            if debug {
                println!("C - applying move at cell {}", cell);
                println!("C - board at cell was: {}", self.board[cell]);
            }

            self.board[cell] = color;

            if debug {
                println!("C - board at cell is now: {}", self.board[cell]);
            }
        }
    }// end apply_move()

    /// Gets a legal move.
    /// 
    /// # Parameters
    /// + **index**: (type: `mut isize`)
    ///   A mutable parameter that is the index of the board array. Used for determining flipped discs.
    /// + **color**: (type: `isize`)
    ///   The current player's color.
    /// + **direction**: (type: `isize`)
    ///   The direction for the index to move in.
    pub fn get_legal_move(&self, mut index: isize, color: isize, direction: isize) -> Move {
        let mut flips: usize = 0;
        let mut legal_move: Move = Move{cell: 0, num_flips: 0, direction: 0};
        let mut break_flag: bool = false;

        while index >= 0 && index < BOARD_SIZE as isize && !break_flag {
            break_flag = check_direction(index, direction);

            index += direction;

            if index >= 0 && index < BOARD_SIZE as isize {
                if self.board[index as usize] != (-color) {
                    break;
                } else {
                    flips += 1;
                }
            } else {
                flips = 0;
                break;
            }
        }// end while loop

        if index >= 0 && index < BOARD_SIZE as isize {
            if self.board[index as usize] == 0 && flips != 0 {
                legal_move.cell = index as usize;
                legal_move.num_flips = flips;
                legal_move.direction = direction;
            }
        }

        legal_move
    }

    /// Generates a vector (similar to a Java list) of all legal moves.
    ///
    /// # Parameters
    /// + **color**: (type: `isize`)
    ///   The color that a player is using to generate the vector of Moves.
    pub fn generate_legal_moves(&self, color: isize) -> Vec<Move> {
        let mut legal_vec: Vec<Move> = Vec::new();

        'cycle: for i in 0..BOARD_SIZE {
            if self.board[i] == color {
                'dirs: for dir in DIRECTIONS.iter() {
                    let temp_legal = self.get_legal_move(i as isize, color, *dir);

                    if temp_legal.num_flips != 0 && !check_border_move(temp_legal) {
                        legal_vec.push(temp_legal);
                    }
                }// end 'dirs loop
            }
        }// end 'cycle loop

        legal_vec
    }// end generate_legal_moves()

    /// An implementation of the minmax search algorithm with alpha-beta pruning.
    /// This implementation of alpha_beta() uses maximum discs flipped as a heuristic.
    /// 
    /// # Parameters
    /// + **is_max_player**: (type: `bool`)
    ///   A boolean value to let the function know if the player is being maximized or not.
    /// + **alpha**: (type: `mut f64`)
    ///   A mutable double-precision floating point number that is the lowest number that can happen.
    /// + **beta**: (type: `mut f64`)
    ///   A mutable double-precision floating point number that is the highest number that can happen.
    /// + **player**: (type: `isize`)
    ///   The current player being evaluated.
    /// + **turn_count**: (type: `u8`)
    ///   The turn count of the game; used for determining how deep to search.
    /// + **depth**: (type: `usize`)
    ///   The current depth of the search.
    #[allow(unused_assignments)]
    pub fn alpha_beta(&mut self,
                       is_max_player: bool,
                       mut alpha: f64,
                       mut beta: f64,
                       player: isize, 
                       depth: usize,
                       // time: u64,
                       turn_count: u8,
                       debug: bool) -> isize 
    {// open alpha_beta funcion body
        // let mut time = self.time_remaining / 10;
        // let atom_bool = AtomicBool::new(false);
        // let arc_time_up = Arc::from(atom_bool);

        let move_count: usize = self.generate_legal_moves(player).len() as usize;
        let mut best_move: isize = 0;
        // let mut time_up: bool = false;
        // let atom_time = AtomicU64::from(time);

        if debug {
            println!("C - move count is: {} | depth is: {}", move_count, depth);
        }// debug check


        if depth == 7_usize {
            // match time_up {
            //     true => println!("C - time ran out on evaluation."),
            //     false => println!("C - hit max depth (10)"),
            // }
            if debug {
                println!("C - hit max depth (7)");
            }

            let scores: Scores = calculate_scores(self.board);
            best_move = scores.score;

            if debug {
                self.print_board();
            }

            // thread::sleep(time::Duration::new(3,0));
        }// if depth hits a maximum

        if depth < 7_usize {
            if is_max_player {
                best_move = isize::min_value();
                let legal_moves: Vec<Move> = self.generate_legal_moves(player);

                'max: for legal in legal_moves.iter() {
                    if debug {
                        println!("C - legal cell = {}", legal.cell);
                    }

                    let mut temp: OthelloBoard = self.clone();
                    temp.apply_move(player, legal.cell, debug);
                    temp.flip_coins(player, -1*legal.direction, legal.cell, debug);

                    let value = temp.alpha_beta(!is_max_player, alpha, beta, -player, depth+1, turn_count, debug);

                    best_move = best_move.max(value);
                    alpha = alpha.max(best_move as f64);

                    if alpha >= beta {
                        break 'max;
                    }
                }// end for loop
            } else {
                best_move = isize::max_value();
                let legal_moves: Vec<Move> = self.generate_legal_moves(player);

                'min: for legal in legal_moves.iter() {
                    if debug {
                        println!("C - legal cell = {}", legal.cell);
                    }

                    let mut temp: OthelloBoard = self.clone();
                    temp.apply_move(player, legal.cell, debug);
                    temp.flip_coins(player, -1*legal.direction, legal.cell, debug);

                    let value = temp.alpha_beta(!is_max_player, alpha, beta, -player, depth+1, turn_count, debug);

                    best_move = best_move.min(value);
                    beta = beta.min(best_move as f64);

                    if alpha >= beta {
                        break 'min;
                    }
                }// end for loop
            }// end if/else branches
        }// if depth hasn't hit max yet

        // this is purposefully left without a semicolon.
        // it's the same as a return statement with a semicolon.
        best_move
    }// end alpha_beta()

    // Think about possibly implementing beam search?

    /// A function to flip the coins on the board.
    /// 
    /// # Parameters
    /// + **color**: (type: `isize`)
    ///   The player color to flip discs to.
    /// + **direction**: (type: `isize`)
    ///   The direction to flip coins in.
    /// + **cell**: (type: `usize`)
    ///   Current index/cell of the board to check flips.
    pub fn flip_coins(&mut self, color: isize, direction: isize, cell: usize, debug: bool) {
        let board_size: isize = 64;
        let mut temp_cell: isize = cell as isize;

        while temp_cell >= 0 && cell < BOARD_SIZE {
            temp_cell = temp_cell + direction;

            if debug {
                println!("C - cell is now: {}", temp_cell);
            }

            if /*cell >= 0 &&*/ temp_cell < board_size {
                if self.board[temp_cell as usize] == color {
                    break;
                } else {
                    self.board[temp_cell as usize] = color;
                }
            }// end overarching if
        }// end while
    }// end flip_coins()

    /// Checks if the game has no more moves and neither player can play any moves.
    pub fn is_game_over(&self) -> bool { self.challenger.passing && self.bot.passing }

    /// Checks and confirms that the game *is* indeed over,
    ///
    /// # Parameters
    /// + **scores**: (type: `Scores`)
    ///   Struct that holds all of the score data, and is used to make sure that whatever
    ///   get input by the opponent is valid and matches what the Score struct contains.
    pub fn confirm_game_over(&self, scores: Scores) {
        if self.is_game_over() {
            loop {
                let mut input: String = String::new();
                io::stdin().read_line(&mut input).expect("C - unable to read user input.");

                // check input to make sure it's an integer;
                // "shadows" (replaces) the `input` variable above.
                let input: isize = match input.trim().parse() {
                    Ok(int) => int,
                    Err(_) => {
                        println!("C - couldn't get an integer.");
                        continue
                    },
                };
                if input != scores.score {
                    panic!("non-matching scores were found.");
                } else {
                    break;
                }
            }
        }
    }// end confirm_game_over()

    /// Prints the board without showing moves.
    pub fn print_board(&self) {
        let get_row = |x| { (x/8)+1 };

        let print_char = |i, s| {
            if i % 8 == 7 {
                print!(" {}\n", s);
            } else if i % 8 != 7 {
                print!(" {}", s); // looks similar to above, but doesn't have a newline
            }
        };

        let color = |n| {
            match n {
                BLACK => "BLACK",
                WHITE => "WHITE",
                _ => "",
            }
        };

        println!("C - (@) is the bot [as {}] | (O) is the challenger [as {}]",
                 color(self.bot.color), color(self.challenger.color));

        // printing the column markers
        println!("C -   ._a_b_c_d_e_f_g_h_");

        for index in 0..BOARD_SIZE {
            if index % 8 == 0 {
                print!("C - {} |", get_row(index));
            }

            match self.board[index] {
                UNFILLED => {
                    print_char(index, "-")
                },
                BLACK => {
                    if self.bot.color == BLACK {
                        print_char(index, "B");
                    } else if self.challenger.color == BLACK {
                        print_char(index, "B");
                    }
                },
                WHITE => {
                    if self.bot.color == WHITE {
                        print_char(index, "W");
                    } else if self.challenger.color == WHITE {
                        print_char(index, "W");
                    }
                },
                _ => {}
            }
        }
    }// end print_board()

    /// Prints the board with moves.
    ///
    /// # Parameters
    /// + **move_list**: (type: `&Vec<Move>`)
    ///   A reference to a Vec<Move> that contains all of the legal moves.
    pub fn print_board_with_moves(&self, move_list: &Vec<Move>) {
        let get_row = |x| { (x/8)+1 };

        let print_char = |i, s| {
            if i % 8 == 7 {
                print!(" {}\n", s);
            } else if i % 8 != 7 {
                print!(" {}", s); // looks similar to above, but has a space
            }
        };

        let color = |n| {
            match n {
                BLACK => "BLACK",
                WHITE => "WHITE",
                _ => "",
            }
        };

        let mut cell_list: Vec<usize> = Vec::new();

        // get the cell values within the vector of moves.
        'get_cells: for _move in move_list {
            cell_list.push(_move.cell);
        }// end 'get_cells for loop

        println!("C - (@) is the bot [as {}] | (O) is the challenger [as {}]",
                 color(self.bot.color), color(self.challenger.color));

        // printing the column markers
        println!("C -   ._a_b_c_d_e_f_g_h_");

        for index in 0..BOARD_SIZE {
            if index % 8 == 0 {
                print!("C - {} |", get_row(index));
            }

            match self.board[index] {
                UNFILLED => {
                    if cell_list.contains(&index) {
                        print_char(index, "+");
                    } else {
                        print_char(index, "-");
                    }
                },
                BLACK => {
                    if self.bot.color == BLACK {
                        print_char(index, "B");
                    } else if self.challenger.color == BLACK {
                        print_char(index, "B");
                    }
                },
                WHITE => {
                    if self.bot.color == WHITE {
                        print_char(index, "W");
                    } else if self.challenger.color == WHITE {
                        print_char(index, "W");
                    }
                },
                _ => {}
            }
        }
    }// end print_board()
}// end impl block for OthelloBoard


/// A function to determine if a move is on one of the side borders, and if its direction is any of
/// the Easts or Wests. Used to prevent that particular move from being added to the legal move list.
fn check_border_move(in_move: Move) -> bool {
    if LEFT_BORDER.contains(&in_move.cell) {
        if in_move.direction == -WEST || in_move.direction == -N_WEST || in_move.direction == -S_WEST {
            return true;
        } else {
            return false;
        }
    } else if RIGHT_BORDER.contains(&in_move.cell) {
        if in_move.direction == -EAST || in_move.direction == -N_EAST || in_move.direction == -S_EAST {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}// end check_border_move()

/// An extra helper function to help with the breaking of the while loop
/// in the `get_legal_move` function above.
fn check_direction(index: isize, direction: isize) -> bool {
    if direction == EAST && RIGHT_BORDER.contains(&(index as usize)) {
        return true;
    } else if direction == WEST && LEFT_BORDER.contains(&(index as usize)) {
        return true;
    } else if direction == N_EAST && (RIGHT_BORDER.contains(&(index as usize)) || TOP_BORDER.contains(&(index as usize))) {
        return true;
    } else if direction == N_WEST && (LEFT_BORDER.contains(&(index as usize)) || TOP_BORDER.contains(&(index as usize))) {
        return true;
    } else if direction == S_EAST && (RIGHT_BORDER.contains(&(index as usize)) || BOTTOM_BORDER.contains(&(index as usize))) {
        return true;
    } else if direction == S_WEST && (LEFT_BORDER.contains(&(index as usize)) || BOTTOM_BORDER.contains(&(index as usize))) {
        return true;
    } else {
        return false;
    }
}