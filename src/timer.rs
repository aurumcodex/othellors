//! # timer.rs
//! 
//! A module to assist in time related operations, such as limiting how far the
//! alpha_beta search can take. (to be implemented in later)
//!
//! (I'll be honest, i'm not entirely sure I'll even need this module)
//!
//! Author: Nathan Adams
//! Date last modified: 2019.10.25

// Timer file for board evaluation functions.
// Something to implement later; the bot is working properly.

#[allow(dead_code)]

use std::time::{Duration, Instant};
use std::thread::{sleep};

const MINUTE: Duration = Duration::from_secs(60);

/// A struct to hold data for time-related functions.
/// 
/// # Fields
/// + **start**: (type: `Instant`)
///   The initial starting point of time for the Timer.
/// + **split**: (type: `Some<Instant>`)
///   A split in the Timer. Can be the end of the Timer duration.
/// + **elapsed**: (type: `Duration`)
///   The elapsed amount of time since the initialization of the Timer.
pub struct Timer {
    pub start: Instant,
    pub elapsed: Duration,
    pub running: bool,
    pub time_up: bool
}

impl std::fmt::Display for Timer {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} miliiseconds", self.instant().elapsed_ms())
    }
}

impl Timer {
    /// Genereates a new Timer data type.
    // pub fn new() -> Timer {
    //     Timer {
    //         start: None,
    //         elapsed: Duration::from_secs(0),
    //         running: true,
    //         time_up: false
    //     }
    // }// end new()

    // /// Starts the Timer.
    // pub fn start(&mut self) { 
    //     self.start = Instant.now();
    //     self.running = true;
    // }

    // pub fn instant(&self) -> Instant {self.start}

    // /// Stops the Timer.
    // pub fn stop(&mut self) { self.start = None; }

    // /// Resets the Timer.
    // pub fn reset(&mut self) {
    //     self.stop();
    //     self.elapsed = Duration::from_secs(0);
    //     self.running = false;
    //     self.time_up = false;
    // }

    // /// Return the Duration of the Timer so far.
    // pub fn duration(&self) -> Duration { 
    //     self.elapsed = self.start.elapsed();
    //     self.elapsed
    // }

    // /// Checks to see if time has run out on a task (default is 1 minute).
    // pub fn time_up(&mut self) -> bool {
    //     if self.elapsed < MINUTE {
    //         self.time_up = false
    //     } else if self.elapsed >= MINUTE {
    //         self.time_up = true;
    //     }

    //     self.time_up
    // }
}// end Timer impl block