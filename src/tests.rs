//! # tests.rs
//!
//! A module to assist in testing some of the code in other files (that *can* be tested).
//!
//! Author: Nathan Adams
//! Date last modified: 2019.10.25

// #[path = "timer.rs"]
// pub mod timer;

#[cfg(test)]
pub mod board_testing {
    use crate::board::{*};
    use crate::values::{WHITE, BLACK, UNFILLED};
    // use crate::timer::{*};

    #[test]
    #[ignore]
    fn check_apply_move() {
        let empty: Vec<usize> = Vec::new();

        let mut _board = OthelloBoard::new();
        _board.gen_board(WHITE);

        let mut legal_moves = _board.generate_legal_moves(BLACK);

        println!("C - enter move: (as a black player)");
        let _move = _board.challenger.get_input(empty);
        _board.apply_move(_board.challenger.color, _move);
        assert_eq!(_board.board[0], UNFILLED);
        assert_eq!(_board.board[_move], _board.challenger.color);
    }// check apply_move


    #[test]
    #[ignore]
    fn check_flip_discs() {
        let debug = false;
        let color = |num: isize| -> &str {
            match num {
                BLACK => "B",
                WHITE => "W",
                _ => " ",
            }
        };

        let full_color = |num: isize| -> &str {
            match num {
                BLACK => "black",
                WHITE => "white",
                _ => "",
            }
        };

        let column = |num: usize| -> &str {
            match num % 8 {
                0 => "a",
                1 => "b",
                2 => "c",
                3 => "d",
                4 => "e",
                5 => "f",
                6 => "g",
                7 => "h",
                _ => "_",
            }
        };

        let row = |num: usize| -> &str {
            match num / 8 {
                0 => "1",
                1 => "2",
                2 => "3",
                3 => "4",
                4 => "5",
                5 => "6",
                6 => "7",
                7 => "8",
                _ => "_",
            }
        };

        let direction = |dir: isize| -> &str {
            match dir {
                NORTH => "North",
                SOUTH => "South",
                WEST => "West",
                EAST => "East",
                N_EAST => "North East",
                N_WEST => "North West",
                S_EAST => "South East",
                S_WEST => "South West",
                _ => "(not a valid direction)",
            }
        };

        let mut othello = OthelloBoard::new();
        othello.gen_board(BLACK);

        let mut legal_moves = othello.generate_legal_moves(BLACK);

        let mut _move = othello.bot.make_move(&legal_moves, othello.bot.color, othello, debug);
        println!("C - bot chose to play at: {} {} {}", color(othello.bot.color), column(_move), row(_move));
        println!("{} {} {}", color(othello.bot.color), column(_move), row(_move));
        othello.apply_move(othello.bot.color, _move, debug);

        let mut legal_moves1 = othello.generate_legal_moves(WHITE);
        let mut _move1 = othello.bot.make_move(&legal_moves, -othello.bot.color, othello, debug);
        println!("C - bot chose to play at: {} {} {}", color(othello.bot.color), column(_move1), row(_move1));
        println!("{} {} {}", color(-othello.bot.color), column(_move1), row(_move1));
        othello.apply_move(othello.bot.color, _move1, debug);

        for list in &legal_moves {
            if list.cell == _move {
                othello.flip_coins(othello.bot.color, -1*list.direction, list.cell, debug);
            }
        }
        othello.print_board();

        assert_eq!(true,true);

    }// check flip_discs

    // #[test]
    // #[ignore]
    // fn check_timer() {
    //     use crate::timer::{Timer};
    //     let mut timer = Timer::new();

    //     timer.start();
    //     assert_eq!(timer.running, true);

    //     timer.stop();
    //     assert_eq!(timer.running, false);

    //     // timer.duration();
    //     // assert_eq!(, );
    //     println!("duration = {:?}", timer.duration());
    // }
}